$(function(){
	// all menu 
	function allmenu(){
		$('.menu-btn').click(function(){
			$('.menu-dim').addClass('on');
			$('.all-menu').addClass('on');
		});
		$('.ham-close-btn, .menu-dim').click(function(){
			$('.menu-dim').removeClass('on');
			$('.all-menu').removeClass('on');
		}); 
	}
	function allMenuToggle(){
		$('.all-menu .ham-body ul li').each(function(i,v){
			if($(this).hasClass('menuToggle')){
				var liEach = $(this);
				var aOnClick = liEach.find('> a');
				aOnClick.on('click',function(){
					if(!liEach.hasClass('on')){
						if($('.all-menu .ham-body ul li.on').length >= 0){
							$('.all-menu .ham-body ul li.on dl').slideUp();
							$('.all-menu .ham-body ul li.on').removeClass('on');
						}
						liEach.find('dl').slideDown();
						liEach.addClass('on');
					}else{
						//liEach.find('dl').slideUp();
						//liEach.removeClass('on');
					}
				});
			}
		});
	}
	allmenu();
	allMenuToggle();
	//메인에서 작동 S
	if($('#container.main').length >= 1){
		var mainSwiper = new Swiper('.main-slider-wrap .main-slider', {
			loop: true,
			autoplay: true,
			centeredSlides: true,
			slidesPerView: 'auto',
			spaceBetween : 0,
			pagination: {
				el: '.main-slider-wrap .swiper-pagination',
				clickable: true,
			}  
			/*
			navigation: {
				nextEl: '.main-slider-wrap .swiper-next',
				prevEl: '.main-slider-wrap .swiper-prev',
				clickable: true,
			},
			*/
		});
		var middleSwiper = new Swiper('.middle-slider-wrap .middle-slider', {
			loop: true,
			autoplay: true,
			centeredSlides: true,
			slidesPerView: 'auto',
			spaceBetween : 0,
			pagination: {
				el: '.middle-slider-wrap .swiper-pagination',
				clickable: true,
			}  
		});
		$('.middle-slider-wrap .swiper-switch-btns').click(function(){
			if(!$(this).hasClass('paused')){
				middleSwiper.autoplay.stop();
				$(this).addClass('paused');
			}else{
				middleSwiper.autoplay.start();
				$(this).removeClass('paused');
			}
		});
		var storySwiper = new Swiper('.story-slider-wrap .story-slider', {
			loop: false,
			centeredSlides: false,
			slidesPerView: 'auto',
			spaceBetween : 28,
			pagination: {
				el: '.story-slider-wrap .swiper-pagination',
				clickable: true,
			}  
			/*
			navigation: {
				nextEl: '.story-slider-wrap .swiper-next',
				prevEl: '.story-slider-wrap .swiper-prev',
				clickable: true,
			},
			*/
		});
	}
	//메인에서 작동 E
	function listHeightCheck(){
		var lineLi = 2;
		var maxHeight = 0;
		var maxLength = $('.listCheck > li').length;
		$('.listCheck > li').each(function(i,v){
			if(!$(this).hasClass('heightCheck')){
				$(this).addClass('heightCheck');
				if($(this).find('.txt-box').outerHeight() >= maxHeight ) maxHeight = $(this).find('.txt-box').outerHeight();
				if(i % lineLi == lineLi - 1 || i == maxLength - 1){
					$('.listCheck > li.heightCheck:not(".heightSet")').each(function(){
						$(this).find('.txt-box').css({'height': maxHeight + 'px'});       
						$(this).addClass('heightSet'); 
					});
					maxHeight = 0;
				}
			}
		});
	}	
	if($('.listCheck').length >= 1){
		listHeightCheck();
		$('.btn-wrap .more-btn').on('click',function(){
			listHeightCheck();
		});
	}

});
$(document).ready(function(){
	//탭기능
	if($('.tab-area').length >= 1){
		var tabArea = $('.tab-area');
		tabArea.find('.tab-btns a').each(function(i,v){
			var btnIndex = i;
			$(this).on('click',function(){
				if(!$(this).hasClass('on')){
					tabArea.find('.tab-btns a').removeClass('on');
					$(this).addClass('on');
					tabArea.find('.tab-con').removeClass('on');
					tabArea.find('.tab-con').eq(btnIndex).addClass('on');
				}
			});
		});
		//url 체크 후 탭 활성화
		var nowUrl = document.location.href;
		if(nowUrl.indexOf('tabindex=') != -1 && nowUrl.indexOf('indexnum') != -1){
			var tabIndexNum = nowUrl.split('tabindex=')[1];
			tabIndexNum = tabIndexNum.split('indexnum')[0];
			tabIndexNum = Number(tabIndexNum);
			tabArea.find('.tab-btns a').eq(tabIndexNum).click();
		}
	}
	//탭 찾아가기	
	if($('.move-btns').length >= 1){
		$('.move-btns a').each(function(i,v){
			var btnInext = i,
			 para_1 = 'tabindex=',
			 para_2 = 'indexnum',
			 nowHref = $(this).attr('href');
			if(nowHref){
				if(nowHref.indexOf(para_1) == -1 && nowHref.indexOf(para_2) == -1){
					if(nowHref.indexOf('?') != -1){
						$(this).attr('href',nowHref + '&' + para_1 + btnInext + para_2);
					}else{
						$(this).attr('href',nowHref + '?' + para_1 + btnInext + para_2);
					}
				}
			}
		});
	}
	//셀렉트박스
	if($('.selectBox').length >= 1){
		$('.selectBox').each(function(){
			var selectBox = $(this),
			 targetSelect = selectBox.find('select'),
			 targetLabel = targetSelect.next('label');
			targetSelect.on('change',function(){
				var newValue = $(this).val();
				targetLabel.text(newValue); 
			});
		});
	}
	//토글박스
	if($('.toggleWrap').length >= 1){
		$('.toggleWrap').each(function(){
			var toggleWrap = $(this),
			 toggleBtns = toggleWrap.find('.toggleBtns');
			toggleBtns.on('click',function(){
				var toggleParent = $(this).parent('.toggleEach');
				if(!toggleParent.hasClass('toggleActive')){
					toggleWrap.find('.toggleActive .toggleBox').stop().slideUp();
					toggleWrap.find('.toggleEach').removeClass('toggleActive');
					toggleParent.find('.toggleBox').stop().slideDown();
					toggleParent.addClass('toggleActive');
				}else{
					toggleParent.find('.toggleBox').stop().slideUp();
					toggleParent.removeClass('toggleActive');
				}
			});
		});
	}					
	//add탭기능
	function tabInSlider(targetClass){
		var targetClass = '.' + targetClass;
		var presentationSwiper = new Swiper(targetClass + ' .presentation-lists .swiper-container', {
			loop: false,
			autoplay: false,
			autoHeight: true,
			centeredSlides: true,
			spaceBetween : 0,
			navigation: {
				nextEl: targetClass + ' .date-set-btn-next',
				prevEl: targetClass + ' .date-set-btn-prev',
				clickable: true,
			},
		});
	}	
	if($('.add-tab').length >= 1){
		$('.add-tab').each(function(){
			var addTab = $(this);
			addTab.find('.add-tab-btns div.swiper-slide').each(function(i,v){
				var btnIndex = i;
				if(btnIndex == 0){
					addTab.find('.add-tab-btns div.swiper-slide').removeClass('on');
					$(this).addClass('on');
					addTab.find('.add-tab-con').removeClass('on');
					addTab.find('.add-tab-con').eq(btnIndex).addClass('on');
					if(!addTab.find('.add-tab-con').eq(btnIndex).hasClass('sliderActive')){
						var addConArea = addTab.find('.add-tab-con').eq(btnIndex);
						if(addConArea.find('.tabInSlider').length >= 1){
							var newClass = 'tabInSlider' + btnIndex;
							addConArea.addClass(newClass);
							tabInSlider(newClass);
							addConArea.addClass('sliderActive');
						}
					}
				}
				$(this).on('click',function(){
					if(!$(this).hasClass('on')){
						addTab.find('.add-tab-btns div.swiper-slide').removeClass('on');
						$(this).addClass('on');
						addTab.find('.add-tab-con').removeClass('on');
						addTab.find('.add-tab-con').eq(btnIndex).addClass('on');
						if(!addTab.find('.add-tab-con').eq(btnIndex).hasClass('sliderActive')){
							var addConArea = addTab.find('.add-tab-con').eq(btnIndex);
							if(addConArea.find('.tabInSlider').length >= 1){
								var newClass = 'tabInSlider' + btnIndex;
								addConArea.addClass(newClass);
								tabInSlider(newClass);
								addConArea.addClass('sliderActive');
							}
						}
					}
				});
			});
		});
	}
});
	
$(window).on('load',function(){
});

$(window).scroll(function(){
});

$(window).on('resize',function(){
});







